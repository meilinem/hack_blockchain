import subprocess
import os

# not persistant.
def get_decentralized_data(certifHash):
	args = {'/usr/local/bin/ipfs', 'cat', "/ipfs/" + certifHash}
	popen = subprocess.Popen(args, stdout=subprocess.PIPE)
	popen.wait()
	output = popen.stdout.read()
	print(output)
	return output

#get_decentralized_data('QmQw8V46Mscb9UC7uLChiTWe1GvEcafhPqwF5HnDHDDkw5')

def call_ipfs_decentralized_data(certifHash):
	cmd = '/usr/local/bin/ipfs cat /ipfs/' + certifHash
	print(os.system(cmd))

call_ipfs_decentralized_data('QmTdUZJmVTH5vd3AiNkeMNcPn4QG2yNhhzGYiviEfRtSpR')
