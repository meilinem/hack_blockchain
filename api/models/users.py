user_candidate = {
    "type": "candidate",
    "info": {
        "name": "",
        "surname": "",
        "age": 0,
        "birthdate": "",
        "location": "",
        "validation": False
    },
    "certification_keys": [],
    "wallet_id": "",
    "requests": [
        {
            "name": "Master2 - Sciences economiques et sociales",
            "cert_id": "1b",
            "status": "validated"
        }
    ]
}

user_certifier = {
    "type": "certifier",
    "info": {
        "name": "",
        "location": {
            "city": "",
            "country": "",
            "zip": ""
        },
        "validation": False
    },
    "wallet_id": "",
    "certification_key": "",
    "requests": [
        {
            "name": "Master2 - Sciences economiques et sociales",
            "candidate_id": "1b",
            "candidate_name": "Julien Dupont",
            "status": "validated"
        }
    ]

}
