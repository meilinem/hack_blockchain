import requests

#data = {
#    "type": "candidates",
#    "info": {
#        "name": "Julian",
#        "surname": "Schapink",
#        "age": 25,
#        "location": {
#            "country": "France",
#            "city": "Saint-Ouen",
#            "zip": "93400"
#        },
#        "birth_place": "Annecy",
#        "validation": False
#    },
#    "certification_keys": [],
#    "wallet_id": "",
#    "requests": []
#}
#r = requests.post('http://10.18.195.181:5000/users/candidates', json=data)

data = {
    "type": "certifiers",
    "info": {
        "name": "42 Paris",
        "location": {
            "country": "France",
            "city": "Paris",
            "zip": "75017"
        },
        "contact": {
            "phone": "0145862511",
            "mail": "contact@42.fr"
        },
        "admin": {
            "siret": ""
        },
        "validation": True
    },
    "certification_keys": [],
    "wallet_id": "",
    "requests": []
}
r = requests.post('http://10.18.195.181:5000/users/certifiers', json=data)

