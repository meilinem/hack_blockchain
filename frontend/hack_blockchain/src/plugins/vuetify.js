import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
    iconfont: 'mdi',
	theme: {
		themes: {
			light: {
                champagne: "#F6E6CA",
                deep_champagne: "#EFD09E",
                pale_spring_bud: "#D2D8B3",
                eerie_black: "#272727",
                p_blue: "#90A9B7",
                dark_p_blue: "#618194",
                real_light_grey: "#fafafa",
                blue_blue: "#56C1FF"
			},
            dark: {
            }
		},
	},
});
