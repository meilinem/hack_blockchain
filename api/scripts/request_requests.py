import requests

data = {
    "author": "8e1833fe-70d7-4538-b855-ec029a41858b",
    "cert_id": "0dbf8a73-c18a-419e-be18-eb9ecf9420d5",
    "cand_id": "8e1833fe-70d7-4538-b855-ec029a41858b",
    "info": {
        "name": "Participation Hackathon Starton",
        "description": "Integration du Web 3.0 dans une interface Web 2.0",
        "mention": "",
        "type": "competitive_event",
        "duration": {
            "start": "03-12-2021",
            "end": "05-12-2021"
        },
        "status": "pending_review"
    }
}
r = requests.post('http://10.18.195.181:5000/requests', json=data)

print(r.text)
