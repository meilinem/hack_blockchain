from __future__ import annotations
from flask import Flask
from redis import Redis
from flask import request, jsonify
from libs.es_handler import EsObject
from flask_restplus import Resource, Api
from flask_cors import CORS, cross_origin
from decouple import config
import tools.create_contract as certif
import json

app = Flask(__name__)
api = Api(app)
CORS(app)
app.config["DEBUG"] = True

auth_token = config('AUTH_TOKEN')
hed = {'Authorization': 'Bearer ' + auth_token}

@api.route('/users/<string:user_type>')
class Users(Resource):
    def __init__(self, api=None) -> None:
        self.api = api
        self._es = EsObject()
        try:
            self._es._create_index(indices=["candidates", "certifiers"])
        except:
            pass

    def get(self, user_type: str) -> tuple:
        try:
            data = self._es._get_documents(index=user_type)
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500
        return {"status": "success", "data": data}, 200

    def post(self, user_type: str) -> tuple:
        try:
            data = request.get_json()
            self._es._create_document(index=user_type, body=request.get_json())
        except:
            return {"status": "error"}, 500
        return {"status": "success"}, 200


@api.route('/requests')
class Requests(Resource):
    def __init__(self, api=None) -> None:
        self.api = api
        self._es = EsObject()

    def post(self) -> tuple:
        try:
            self._es._create_index(indices=["requests"])
        except:
            pass

        data = request.get_json()
        try:
            return self._es._new_request(candidate=data["cand_id"],
                                         certifier=data["cert_id"],
                                         cert_request=data), 200
        except Exception as e:
            return {"status": "error", "data": str(e)}, 200

    def get(self) -> tuple:
        try:
            data = self._es._get_documents(index="requests")
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500
        return {"status": "success", "data": data}, 200


@api.route('/requests/<string:id>')
class RequestsID(Resource):
    def __init__(self, api=None) -> None:
        self.api = api
        self._es = EsObject()

    def get(self, id: str) -> tuple:
        try:
            return self._es._get_document_by_id(index="requests", id=id)
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500

    def delete(self, id: str) -> tuple:
        try:
            r = self._es._get_document_by_id(index="requests", id=id)
            source = r[0]["data"]["hits"]["hits"][0]["_source"]
            cert_id = source["cert_id"]
            cand_id = source["cand_id"]
            self._es._delete_request(index="certifiers", id=cert_id, request_id=id)
            self._es._delete_request(index="candidates", id=cand_id, request_id=id)
            self._es._delete_document_by_id(index="requests", id=id)

            return {"status": "success", "data": source}, 200
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500


@api.route('/requests/<string:id>/<string:status>')
class RequestsIDStatus(Resource):
    def __init__(self, api=None) -> None:
        self.api = api
        self._es = EsObject()

    def post(self, id: str, status: str):
        if status == "rejected":
            return self._es._update_request_status(status=status, request_id=id)
        elif status == "validated":
            req = self._es._get_document_by_id(index="requests", id=id)
            tmp = json.dumps(req[0]["data"]["hits"]["hits"][0]["_source"]["info"]["name"])
            data = request.get_json()
            #add request bdd for wallet ids and payload content
            certif.post_certif([data["cand_wallet"], data["cert_wallet"], tmp])
            return self._es._update_request_status(status=status, request_id=id)
        else:
            return {"status": "error", "data": "wrong status"}


@api.route('/users/<string:user_type>/<string:id>', methods=["GET", "PATCH", "DELETE"])
class UsersID(Resource):
    def __init__(self, api=None) -> None:
        self.api = api
        self._es = EsObject()
        try:
            self._es._create_index(indices=["candidates", "certifiers"])
        except:
            pass

    def get(self, user_type: str, id: str) -> tuple:
        try:
            return self._es._get_document_by_id(index=user_type, id=id)
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500


    def patch(self, user_type: str, id: str) -> tuple:
        try:
            data = request.get_json()
            data = data["wallet_id"]
            return self._es._patch_user_wallet(index=user_type, id=id,
                                               wallet_id=data)
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500


    def delete(self, user_type: str, id: str) -> tuple:
        try:
            return self._es._delete_document_by_id(index=user_type, id=id)
        except Exception as e:
            return {"status": "error", "data": str(e)}, 500


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
