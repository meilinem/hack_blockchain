import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import createPersistedState from "vuex-persistedstate";


export default new Vuex.Store({
  plugins: [createPersistedState({
    paths: ['user_id', 'user_type']
  })],
  state: {
    cend_user: {
      "login": "jschapin",
      "id": "8e1833fe-70d7-4538-b855-ec029a41858b"
    },
    cert_users: {
      "uit_lyon": "1b887343-6c77-4cee-a835-ff0d5d2885a2",
      "insa_lyon": "e41bd89f-0118-45e5-b981-537bb4254e17",
      "c_baudelaire": "33907da8-e208-4cfc-bc44-a03c2ec1f445",
      "ntn_snr": "9d57ff45-4e25-414f-aede-afbae82bc2a0",
      "42_paris": "592a802a-f6fc-4109-9f2b-8cb12e784ae5"
    },
    user_type_index: "",
    user_id: "",
    tabs: [
      {
        "id": 0,
        "theme": "curriculum vitae",
        "name": "Curriculum",
        "type": "third party",
        "component_name": "CandidateRecap",
        "active": true
      },
      {
          "id": 1,
          "theme": "notarial",
          "name": "Notary",
          "type": "bilateral",
          "component_name": "NotaryRecap",
          "active": false
      },
      {
          "id": 2,
          "theme": "ticketing",
          "name": "Ticketing",
          "type": "third party",
          "component_name": "TicketingRecap",
          "active": false
      },
      {
          "id": 3,
          "theme": "loaning",
          "name": "Loaning",
          "type": "bilateral",
          "component_name": "LoaningRecap",
          "active": false
      }
    ]
  },
  mutations: {
    user_id: (state, newValue) => {
      state.user_id = newValue
    },
    handle_status: (state, newValue) => {
      for (let i in state.tabs) {
          if (i == newValue.id)
              state.tabs[i].active = newValue.active
      }
    },
    user_type_index: (state, newValue) => {
      state.user_type_index = newValue
    }
  },
  actions: {
  },
  modules: {
  }
})
