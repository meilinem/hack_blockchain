from __future__ import annotations
from elasticsearch import Elasticsearch
import uuid

es_indices = ["users", "requests"]

config = {
    "host": "10.18.195.181",
    "port": 9200
}

es = Elasticsearch([config,], timeout=300)


class EsObject(object):
    def __init__(self):
        pass

    def _create_index(self, indices:list=None) -> tuple:
        try:
            if indices is None:
                indices = es_indices
            for index in indices:
                es.indices.create(index=index)
        except Exception as e:
            print(f'{str(e)}')
            return {"status": "error"}, 500

        return {"status": "success"}, 200

    def _create_document(self, index: str, body: dict) -> tuple:
        try:
            r = es.index(
                refresh=True,
                index = index,
                doc_type = 'doc',
                id = uuid.uuid4(),
                body = body
            )
        except Exception as e:
            print(f'{str(e)} ')
            return {"status": "error"}, 500
        return {"status": "success", "data": r}, 200

    def _get_documents(self, index: str) -> tuple:
        try:
            r = es.search(size=1000, index=index, query={"match_all": {}})
        except Exception as e:
            print(f'{str(e)}')
            return {"status": "error"}, 500
        return {"status": "success", "data": r}, 200

    def _delete_document_by_id(self, index: str, id: str) -> tuple:
        es.delete(index=index, id=id)

    def _delete_request(self, index: str, id: str, request_id: str) -> tuple:
        r = es.update(
            index=index,
            doc_type="doc",
            id=id,
            body={
                "script": {
                    "source": """
                        for (int i=ctx._source.requests.length-1; i>=0; i--) {
                            if (ctx._source.requests[i] == params.request_id) {
                                ctx._source.requests.remove(i);
                            }
                        }""",
                    "params": {
                        "request_id": request_id
                    }
                }
            })

    def _get_document_by_id(self, index: str, id: str) -> tuple:
        try:
            r = es.search(
                size=1000,
                index=index,
                query={
                    "bool": {
                        "must": {
                            "match": {
                                "_id": id
                            }
                        }
                    }
            })
        except Exception as e:
            print(f'{str(e)}')
            return {"status": "error"}, 500
        return {"status": "success", "data": r}, 200

    def _patch_user_wallet(self, index: str, id: str, wallet_id: str):
        r = es.update(
            index=index,
            doc_type='doc',
            id=id,
            body={
                "doc" : {
                    "wallet_id": wallet_id
                }
            })
        return r, 200


    def _update_request_status(self, status: str, request_id: str) -> tuple:
        r = es.update(
            index="requests",
            doc_type='doc',
            id=request_id,
            body={
                "doc" : {
                    "info": {
                        "status": status
                    }
                }
            })
        return r, 200


    def _update_request(self, index: str, doc_id: str, request_id: str) -> dict:
        r = es.update(
            index=index,
            doc_type='doc',
            id=doc_id,
            body={
                "script" : {
                    "source": "ctx._source.requests.addAll(params.requests)",
                    "lang": "painless",
                    "params" : {
                        "requests" : [request_id]
                    }
                }
            })

    def _new_request(self, candidate: str, certifier: str, cert_request: dict) -> dict:
        r = self._create_document(index="requests", body=cert_request)
        request_id = r[0]["data"]["_id"]

        up_candidate = self._update_request(index="candidates",
                                            doc_id=candidate,
                                            request_id=request_id)
        up_certifier = self._update_request(index="certifiers",
                                            doc_id=certifier,
                                            request_id=request_id)
        return {"status": "success"}
