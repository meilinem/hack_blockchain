# hack_blockchain

## Getting started

### Database + API
```
	$> docker-compose build
	$> docker-compose up
```

### Frontend
```
	$> cd frontend/hack_blockchain/
	$> npm install
	$> npm run dev
```
