import Vue from 'vue'
import VueRouter from 'vue-router'
import Test from '../views/Test.vue'
import Home from '../views/Home.vue'
import CertifierDashboard from '../views/CertifierDashboard.vue'
import CandidateDashboard from '../views/CandidateDashboard.vue'
import CandidateRecap from '../views/CandidateRecap.vue'
import CertifierRecap from '../views/CertifierRecap.vue'
import Settings from '../views/Settings.vue'
import WalletID from '../views/WalletID.vue'



Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  },
  {
    path: '/certifier/dashboard',
    name: 'CertifierDashboard',
    component: CertifierDashboard
  },
  {
    path: '/candidate/dashboard',
    name: 'CandidateDashboard',
    component: CandidateDashboard
  },
  {
    path: '/candidate/:id/recap',
    name: 'CandidateRecap',
    component: CandidateRecap
  },
  {
    path: '/certifier/:id/recap',
    name: 'CertifierRecap',
    component: CertifierRecap
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/wallet_id',
    name: 'WalletID',
    component: WalletID
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
