from decouple import config
import requests
import json
#import filter_contract as filt

header={"x-api-key": config('API_KEY')}

def get_witnessed_contracts(lst:list, filter) :
    witnessed = []
    for i in lst :
        if i[1] == filter :
            witnessed.append(i)
    return witnessed

def get_contract() :
    r = requests.get('https://api-connect.starton.io/v1/smart-contract/'+ config('CONTRACT_ADDRESS') + '/functions', headers=header)
    return(r.content)

def post_certif(lst: list):
    if not isinstance(lst, list) or not len(lst) == 3:
        raise ValueError("Must be a list of len equals to 3.")
    payload = {"functionName": "create_contract", "params": lst}
    rpost = requests.post('https://api-connect.starton.io/v1/smart-contract/' + config('CONTRACT_ADDRESS') + '/interact', headers=header, json=payload)
    return rpost.text

def get_certif(user_address):
    if not isinstance(user_address, str):
        raise ValueError("Address must be a string")
    payload = {"functionName": "get_certif_user", 'params':[user_address]}
    rpost = requests.post('https://api-connect.starton.io/v1/smart-contract/' + config('CONTRACT_ADDRESS') + '/read', headers=header, json=payload)
    rpost_parsed = parse_certifictations(json.loads(rpost.text))
    certified = get_witnessed_certification(rpost_parsed)
    return(certified)

def parse_certifictations(gcert:json):
    lst = gcert['response'].split(',')
    resume_of_certifs = []
    for i in range(0, len(lst), 3):
        tmp = []
        for j in range(i, (3 + i)):
            tmp.append(lst[j])
        resume_of_certifs.append(tmp)
    return resume_of_certifs

def get_witnessed_certification(lst:list) :
    contracts = get_witnessed_contracts(lst, config('WITNESS_ID'))
    return contracts
